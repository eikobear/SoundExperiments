require 'roda'

Rack::Mime::MIME_TYPES.merge!({ '.mjs' => 'application/javascript; charset=utf-8' })

class App < Roda

  plugin :render
  plugin :public

  route do |r|

    r.root do
      view :index
    end

#    r.on "buffer" do
#      view :buffer
#    end

    r.public

  end

end

run App.freeze.app
