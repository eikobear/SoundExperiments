
export default class Voice {
  constructor(context, options) {
    // TODO: create a defaults object and merge it with options
    this.context = context;
    this.delay = options.delay || 0;
    this.duration = options.duration || 1;
    this.volume = options.volume || 0.5;
    this.fadein = options.fadein || 0;
    this.fadeout = options.fadeout || 0;
    this.oscillator = this.context.createOscillator();
    this.gain = this.context.createGain();
    this.oscillator.type = options.type || 'sine';
    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime);
    this.oscillator.connect(this.gain);
    this.gain.connect(this.context.destination);
    this.oscillator.start(this.context.currentTime);
  }

  play(frequency) {

    this.gain.gain.cancelScheduledValues(this.context.currentTime);

    this.oscillator.frequency.setValueAtTime(frequency, this.context.currentTime);
    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime);

    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime + this.delay);

    this.gain.gain.exponentialRampToValueAtTime(this.volume, this.context.currentTime + this.delay + this.fadein);
    this.gain.gain.setValueAtTime(this.volume, this.context.currentTime + (this.duration) - this.fadeout);
    this.gain.gain.exponentialRampToValueAtTime(0.00001, this.context.currentTime + (this.duration));
  }

  loop(bpm) {
    const duration = 60.0 / bpm;

    this.loop = setInterval(() => {
      this.play(Voice.note2Frequency('a4'));
    }, duration * 1000);
  }

  riff(bpm, notestring) {
    const duration = 60.0 / bpm * 1000;
    const notes = notestring.split(' ');
    let unplayed = [...notes];

    this.riff = setInterval(() => {
      this.play(Voice.note2Frequency(unplayed.shift()));
      if(unplayed.length === 0) unplayed = [...notes];
    }, duration);
  }

  endLoop() {
    if(this.loop != null) clearInterval(this.loop);
    this.loop = null;
  }


  endRiff() {
    if(this.riff != null) clearInterval(this.riff);
    this.riff = null;
  }

  static note2Frequency(note) {
    const keys = 'cCdDefFgGaAb';
    const bkey = 'a';
    const boctave = 4;
    const bfrequency = 440.0;
    let [key, octave] = note;

    octave = Number(octave);

    let halfsteps = keys.indexOf(key) - keys.indexOf(bkey);
    halfsteps += (octave - boctave) * 12;
    
    let a = 2 ** (1 / 12);
    let frequency = bfrequency * (a ** halfsteps);
    return frequency;
  }
}

