
import Voice from './voice.mjs';

const init = function init() {
  const context = new AudioContext();
  const voice = new Voice(context, {
    delay: 0,
    duration: 0.9,
    fadein: 0.01,
    fadeout: 0.8,
    volume: 0.2,
    type: 'sine',
  });
  voice.riff(200, 'c4 d4 e4 f4 g4 a4 b4 c5');
};

if (document.readyState === 'complete') {
  init();
} else {
  document.addEventListener('DOMContentLoaded', init);
}


